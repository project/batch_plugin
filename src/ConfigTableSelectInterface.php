<?php

namespace Drupal\batch_plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Config table select interface.
 */
interface ConfigTableSelectInterface {

  /**
   * Add a table select.
   *
   * @param array $form
   *   The form API parent form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param mixed $table_rows_key
   *   The table rows key.
   * @param mixed $config_key
   *   The config key.
   */
  public function addTableSelect(array &$form, FormStateInterface $form_state, $table_rows_key = '', $config_key = '');

  /**
   * Get the table headers.
   *
   * @param string $table_options_config_key
   *   The config key.
   *
   * @return array
   *   The headers.
   */
  public function getHeaders($table_options_config_key);

  /**
   * Add the row - here to allow overrides if required.
   *
   * @param mixed $oid
   *   The option ID.
   * @param array $option
   *   The option columns.
   *
   * @return array
   *   The option row.
   */
  public function addRow($oid, array $option);

}
