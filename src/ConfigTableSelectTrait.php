<?php

namespace Drupal\batch_plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Table select trait for batch plugin entity config forms.
 */
trait ConfigTableSelectTrait {

  /**
   * Table rows configuration key.
   *
   * @var mixed
   */
  protected $tableRowsConfigKey = 'table_select_rows';

  /**
   * The configuration key.
   *
   * @var mixed
   */
  protected $tableSelectConfigKey = 'table_select_config';

  /**
   * Add a table select.
   *
   * @param array $form
   *   The form API parent form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param mixed $table_rows_key
   *   The table rows key.
   * @param mixed $config_key
   *   The config key.
   */
  public function addTableSelect(array &$form, FormStateInterface $form_state, $table_rows_key = '', $config_key = '') {
    // Set up some default config.
    if (empty($table_rows_key)) {
      $table_rows_key = $this->tableRowsConfigKey;
    }
    if (empty($config_key)) {
      $config_key = $this->tableSelectConfigKey;
    }
    // Get the headers and return if we don't have any.
    $headers = $this->getHeaders($table_rows_key);
    if (empty($headers)) {
      return;
    }
    // Add the rows.
    $row_options = $this->configuration[$table_rows_key];
    foreach ($row_options as $roid => $row_option) {
      $rows[$roid] = $this->addRow($roid, $row_option);
    };
    // The config key.
    $form[$config_key] = [
      '#type' => 'tableselect',
      '#header' => $headers,
      '#options' => $rows,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration[$config_key] ?? [],
    ];
  }

  /**
   * Get the table headers.
   *
   * @param string $table_options_config_key
   *   The config key.
   *
   * @return array
   *   The headers.
   */
  public function getHeaders($table_options_config_key) {
    $source = $this->getConfiguration()[$table_options_config_key] ?? [];
    if (empty($source)) {
      return [];
    }
    $first = reset($source);
    $keys = array_keys($first);
    foreach ($keys as $key) {
      $display = ucwords(str_replace('_', ' ', $key));
      $headers[$key] = $display;
    }
    return $headers;
  }

  /**
   * Add the row - here to allow overrides if required.
   *
   * @param mixed $oid
   *   The option ID.
   * @param array $option
   *   The option columns.
   *
   * @return array
   *   The option row.
   */
  public function addRow($oid, array $option) {
    return $option;
  }

  /**
   * {@inheritDoc}
   */
  public function configTableSelectTraitFinished(bool $success, array $results, array $operations): void {
    $config = \Drupal::service('config.factory')->getEditable($this->getConfigKey());
    $settings = $config->get('settings');
    $settings[$this->tableRowsConfigKey] = $results[$this->tableRowsConfigKey];
    $config->set('settings', $settings)->save();
  }

}
