<?php

namespace Drupal\batch_plugin\Plugin\Processor;

/**
 * Plugin implementation of the processor.
 *
 * @Processor(
 *   id = "drush",
 *   label = @Translation("Drush"),
 *   description = @Translation("Drush.")
 * )
 */
class Drush extends BatchApi {
  // Same as Batch API for Drush jobs.
}
