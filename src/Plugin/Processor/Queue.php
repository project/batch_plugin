<?php

namespace Drupal\batch_plugin\Plugin\Processor;

use Drupal\batch_plugin\QueueProcessorPluginBase;

/**
 * Plugin implementation of the processor.
 *
 * @Processor(
 *   id = "queue",
 *   label = @Translation("Queue"),
 *   description = @Translation("Queue.")
 * )
 */
class Queue extends QueueProcessorPluginBase {

}
